import React from 'react';
import { useSelector } from 'react-redux';
import CardItem from '../../components/CardItem';
import styles from './Favorite.module.scss';

function Favorite() {
  const items = useSelector((state) => state.items.data);
  const favorite = items.filter((item) => item.favourite);


   if (favorite.length < 1) {
     return <h2 className={styles.empty}>Add your favorite products</h2>;
   }

  return (
    <>
      <h2>Favorite</h2>
      <div className={styles.favorite}>
        {favorite.map((item) => {
          return <CardItem key={item.path} item={item} toCart />;
        })}
      </div>
    </>
  );
}

export default Favorite;
