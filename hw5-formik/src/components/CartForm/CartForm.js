import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import { useSelector } from 'react-redux';
import styles from './CartForm.module.scss';

export const CartForm = () => {
  const cartStore = useSelector((state) => state.items.data);
  const cartStoreNew = cartStore.filter((el) => el.inCart);

  const { formContainer, formField } = styles;

  const SignupSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('Name field is requierd'),
    lastName: Yup.string()
      .min(2, 'Too Short!')
      .max(50, 'Too Long!')
      .required('lastName field is requierd'),
    age: Yup.number().required('Age field is requierd').positive().integer(),
    adress: Yup.string().required('Adress field is requierd'),
    phone: Yup.number().required('Invalid email phone').positive(),
    email: Yup.string().email('Invalid email').required('Required'),
  });

  const handleSubmit = (values, actions) => {
    console.log('Buyer =', values);
    console.log('You bought: ', cartStoreNew);
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
      actions.setSubmitting(false);
    }, 1000);
  };

  const clearLocalStorage = () => {
    localStorage.removeItem('addCart');
  };

  return (
    <div className={formContainer}>
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          age: '',
          adress: '',
          phone: '',
          email: '',
        }}
        validationSchema={SignupSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, touched }) => (
          <Form className={formField}>
            <Field type="text" name="firstName" placeholder="Name" />
            {errors.firstName && touched.firstName ? (
              <div>{errors.firstName}</div>
            ) : null}
            <Field type="text" name="lastName" placeholder="lastName" />
            {errors.lastName && touched.lastName ? (
              <div>{errors.lastName}</div>
            ) : null}

            <Field type="number" name="age" placeholder="age" />
            {errors.age && touched.age ? <div>{errors.age}</div> : null}

            <Field type="text" name="adress" placeholder="adress" />
            {errors.adress && touched.adress ? (
              <div>{errors.adress}</div>
            ) : null}

            <Field type="text" name="phone" placeholder="phone" />
            {errors.phone && touched.phone ? <div>{errors.phone}</div> : null}

            <Field name="email" type="email" placeholder="email" />
            {errors.email && touched.email ? <div>{errors.email}</div> : null}
            <button type="submit" onClick={clearLocalStorage}>
              Checkout
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};
