import React from 'react';
import { useSelector } from 'react-redux';
import CardItem from '../CardItem';
import styles from './CardContainer.module.scss';

function CardContainer() {
  const cards = useSelector((state) => state.items.data);

  return (
    <>
      <div className={styles.listContainer}>
        <ul className={styles.list}>
          {cards.map((item) => (
            <li key={item.path}>
              <CardItem key={item.path} item={item} toCart />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}

export default CardContainer;
