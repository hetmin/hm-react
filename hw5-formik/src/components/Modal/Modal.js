import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './Modal.module.scss';

function Modal(props) {
  const { info: action, data: item } = useSelector((state) => state.modal);
  const { closeModal, actions } = props;
  const {
    modal,
    modal__body,
    modal__header,
    modal__headerClose,
    modal__main,
    modal__footer,
  } = styles;

  return (
    <div className={modal} onClick={closeModal}>
      <div className={modal__body} onClick={(e) => e.stopPropagation()}>
        <div className={modal__header}>
          <div className={modal__headerClose} onClick={closeModal}>
            ×
          </div>
        </div>

        <div className={modal__main}>{action + ' ' + item.name + '?'}</div>

        <div className={modal__footer}>{actions}</div>
      </div>
    </div>
  );
}

export default Modal;

Modal.propTypes = {
  closeModal: PropTypes.func,
  closeButton: PropTypes.bool,
  actions: PropTypes.array,
};
