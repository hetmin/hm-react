import { CARD } from './types';

export const getCart = () => {
  const addCart = localStorage.getItem('addCart');
  return addCart ? JSON.parse(addCart) : {};
};

export const setCart = (data) => {
  localStorage.setItem('addCart', JSON.stringify(data));
};

export const getFavorite = () => {
  const favouritesLS = localStorage.getItem('Favourite');
  return favouritesLS ? JSON.parse(favouritesLS) : [];
};

export const setFavorite = (data) => {
  localStorage.setItem('Favourite', JSON.stringify(data));
};

export const loadItems = () => async (dispatch) => {
  const favourites = getFavorite();
  const addCart = getCart();

  try {
    const data = await fetch(`./fitness.json`).then((res) => res.json());

    const newCard = data.map((item) => {
      item.favourite = favourites.includes(item.article);
      item.inCart = addCart[item.article] || null;
      return item;
    });
    dispatch({ type: CARD, payload: newCard });
  } catch (error) {}
};
