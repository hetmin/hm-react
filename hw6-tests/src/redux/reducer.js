import {
  CARD,
  SHOW_MODAL,
  FAVORITE,
  CLOSE_MODAL,
  ADD_CART,
  DEl_CART,
} from './types';
import { getCart, setCart, getFavorite, setFavorite } from "./utils";
import produce from "immer";

const initialState = {
  items: {
    data: [],
    isLoading: true,
  },
  modal: {
    info: "",
    data: null,
    isModalOpen: false,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CARD:
      return {
        ...state,
        items: { ...state.data, data: action.payload, isLoading: false },
      };
    case SHOW_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal,
          info: action.payload.info,
          data: action.payload.data,
          isModalOpen: true,
        },
      };
    case CLOSE_MODAL:
      return { ...state, modal: { ...state.modal, isModalOpen: false } };

    case FAVORITE:
      return produce(state, (newState) => {
        newState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.favourite = !el.favourite;

            const favourites = getFavorite();
            if (favourites.includes(el.article)) {
              favourites.splice(favourites.indexOf(el.article), 1);
            } else {
              favourites.push(el.article);
            }
            setFavorite(favourites);
          }
          return el;
        });
      });

    case ADD_CART:
      return produce(state, (newState) => {
        newState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.inCart++;
            const newCart = getCart();
            if (Object.keys(newCart).includes(el.article)) {
              newCart[el.article]++;
            } else {
              newCart[el.article] = 1;
            }
            setCart(newCart);
          }
          return el;
        });
      });

    case DEl_CART:
      return produce(state, (newState) => {
        newState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.inCart--;
            const newCart = getCart();
            newCart[el.article]--;
            if (newCart[el.article] === 0) {
              delete newCart[el.article];
            }
            setCart(newCart);
          }
          return el;
        });
      });

    default:
      return state;
  }
};

export default reducer;
