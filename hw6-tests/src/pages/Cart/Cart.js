import React from 'react';
import { useSelector } from 'react-redux';
import CardItem from '../../components/CardItem';
import styles from './Cart.module.scss';
import { CartForm } from '../../components/CartForm/';

function Cart() {
  const items = useSelector((state) => state.items.data);
  const addedCart = items.filter((item) => item.inCart);

  if (addedCart.length < 1) {
    return <h2 className={styles.empty}>The shopping cart is empty</h2>;
  }

  return (
    <>
      <CartForm />
      <h2>Cart</h2>
      <div className={styles.cart}>
        {addedCart.map((item) => {
          return <CardItem key={item.article} item={item} fromCart />;
        })}
      </div>
    </>
  );
}

export default Cart;
