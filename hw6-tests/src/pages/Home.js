import React from 'react';
import CardContainer from '../components/CardContainer';
import { AppContext } from '../context/Context';
import { useState } from 'react';

function Home() {
  const [tableView, setTableView] = useState(false);
  const changeTypeView = (value) => {
    setTableView(!value);
  };

  const contextValue = {
    tableView,
    changeTypeView,
  };

  return (
    <main>
      <section>
        <AppContext.Provider value={contextValue}>
          <CardContainer />
        </AppContext.Provider>
      </section>
    </main>
  );
}

export default Home;
