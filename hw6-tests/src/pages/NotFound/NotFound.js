import styles from './NotFound.module.scss';

function NotFound() {
  return (
    <div>
      <h2 className={styles.title}>This page is not found</h2>
    </div>
  );
}

export default NotFound;
