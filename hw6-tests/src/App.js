import React from 'react';
import { useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import Button from './components/Button';
import Modal from './components/Modal';
import MainLayout from './layouts/MainLayout';
import Cart from './pages/Cart';
import NotFound from './pages/NotFound';
import Favorite from './pages/Favorite';
import Home from './pages/Home';
import { loadItems } from './redux/utils';
import { ADD_CART, DEl_CART, CLOSE_MODAL } from './redux/types';

function App() {
  const dispatch = useDispatch();
  const isModalOpen = useSelector((state) => state.modal.isModalOpen);
  const infoModal = useSelector((state) => state.modal.info);
  const isLoading = useSelector((state) => state.items.isLoading);
  const thisItem = useSelector((state) => state.modal.data);

  useEffect(() => {
    dispatch(loadItems());
  }, [dispatch]);

  const closeModal = () => {
    dispatch({ type: CLOSE_MODAL });
  };

  const addToCart = (data) => {
    dispatch({ type: ADD_CART, payload: data });
  };

  const DelFromCart = (data) => {
    dispatch({ type: DEl_CART, payload: data });
  };

  if (isLoading) {
    return <div className="App">Loading...</div>;
  }

  return (
    <BrowserRouter>
      {/* <TablContext.Provider value={ contextValue }> */}
      <>
        <section>
          <Routes>
            <Route path="/" element={<MainLayout />}>
              <Route index element={<Home />} />
              <Route path="cart" element={<Cart />} />
              <Route path="favorite" element={<Favorite />} />
              <Route path="*" element={<NotFound />} />
            </Route>
          </Routes>
        </section>

        {isModalOpen && (
          <Modal
            closeButton={true}
            closeModal={closeModal}
            actions={[
              <Button
                key="1"
                onClick={() => {
                  if (infoModal === 'Add to cart') {
                    addToCart(thisItem.article);
                  }
                  if (infoModal === 'Remove from cart') {
                    DelFromCart(thisItem.article);
                  }
                  closeModal();
                }}
                className="button"
                text="Ok"
              />,
              <Button
                key="2"
                onClick={closeModal}
                className="button"
                text="Cancel"
              />,
            ]}
          />
        )}
      </>
      {/* </TablContext.Provider> */}
    </BrowserRouter>
  );
}

export default App;
