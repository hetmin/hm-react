import React from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

function Button(props) {
  const { backgroundColor, text, onClick } = props;
  return (
    <button
      className={styles.button}
      style={{ backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
}

export default Button;

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  text: '',
  onClick: () => {},
};
