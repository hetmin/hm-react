import { fireEvent, render, screen } from '@testing-library/react';
import Button from './Button';

describe('Test Button component', () => {
  test('Test click event', () => {
    render(<Button text="Submit"></Button>);
    const btn = screen.getByText('Submit');
    fireEvent.click(btn);
  });
});

describe('Test render Button', () => {
  test('should button render', () => {
    const { asFragment } = render(
      <Button type={'submit'} backgroundColor="green" text="Submit">
        Click
      </Button>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
