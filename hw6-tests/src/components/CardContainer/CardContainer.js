import React from 'react';
import { useSelector } from 'react-redux';
import { useContext } from 'react';
import CardItem from '../CardItem';
import styles from './CardContainer.module.scss';
import { AppContext } from '../../context/Context';
import Button from '../Button/Button';
// import {AppContext} from '../../App'

function CardContainer() {
  const cards = useSelector((state) => state.items.data);
  const { listContainer, list } = styles;

  const { tableView, changeTypeView } = useContext(AppContext);

  return (
    <>
      <div className={listContainer}>
        <Button onClick={() => changeTypeView(tableView)} text="Change view" />
        <ul className={list}>
          {cards.map((item) => (
            <li key={item.path}>
              <CardItem
                key={item.path}
                item={item}
                toCart
                tableView={tableView}
              />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}

export default CardContainer;
