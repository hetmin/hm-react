import { render, fireEvent, screen } from '@testing-library/react';
import Modal from './Modal';

describe('Modal.js', () => {
  test('should Modal Render', () => {
    render(<Modal className="modalBackground" />);
  });

  test('Try modal Action', () => {
    const action = ['Action'];
    render(<Modal actions={action} />);
  });

  test('modal close ', () => {
    const handleClose = jest.fn();
    const { getByText } = render(
      <Modal onClose={handleClose}>
        <div>test</div>
      </Modal>
    );

    expect(getByText('test')).toBeInTheDocument();
    fireEvent.click(getByText(/close/i));
    expect(handleClose).toHaveBeenCalledTimes(1);
  });

  test('Snapshot', () => {
    const { container } = render(<Modal />);
    expect(container.innerHTML).toMatchSnapshot();
  });
});
