import { useDispatch } from 'react-redux';
import { AiFillHeart } from 'react-icons/ai';
import PropTypes from 'prop-types';
import styles from './CardItem.module.scss';
import Button from '../Button';
import { SHOW_MODAL, FAVORITE } from '../../redux/types';

const CardItem = ({ item, toCart, fromCart }) => {
  const { name, path, article, price, favourite } = item;
  const { card, img, title, btnPrice, priceCard, isHeart, heart } = styles;

  const dispatch = useDispatch();

  const addToFavourite = (id) => {
    dispatch({ type: FAVORITE, payload: id });
  };

  const showModal = (info, data) => {
    dispatch({ type: SHOW_MODAL, payload: { info, data } });
  };

  return (
    <div className={card}>
      <AiFillHeart
        className={favourite ? isHeart : heart}
        onClick={() => addToFavourite(article)}
      />
      <h1 className={title}>{name}</h1>;
      <img className={img} src={path} alt={name} />
      <div className={btnPrice}>
        <div className={priceCard}>{price} грн</div>
        <div>
          {toCart && (
            <Button
              onClick={() => {
                showModal('Add to cart', item);
              }}
              text={'Add to cart'}
            />
          )}
          {fromCart && (
            <Button
              onClick={() => {
                showModal('Remove from cart', item);
              }}
              text={'Delete'}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default CardItem;

CardItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    path: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    favourite: PropTypes.bool,
  }),
  toCart: PropTypes.bool,
  fromCart: PropTypes.bool,
};
