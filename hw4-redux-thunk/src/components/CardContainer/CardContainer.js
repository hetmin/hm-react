import React from 'react';
import CardItem from '../CardItem';
import styles from './CardContainer.module.scss';
import { useSelector } from 'react-redux';

function CardContainer() {
  const cards = useSelector((state) => state.items.data);

  return (
    <div>
      <ul className={styles.list}>
        {cards.map((item) => (
          <li key={item.path}>
            <CardItem key={item.path} item={item} toCart />
          </li>
        ))}
      </ul>
    </div>
  );
}

export default CardContainer;
