import React from 'react';
import CardContainer from '../components/CardContainer';

function Home() {
  return (
    <main>
      <section>
        <CardContainer />
      </section>
    </main>
  );
}

export default Home;
