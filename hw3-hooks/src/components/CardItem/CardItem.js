import React from 'react';
import { AiFillHeart } from 'react-icons/ai';
import styles from './CardItem.module.scss';
import Button from '../Button';

function CardItem(props) {
  const { cardItem, onClose, setModalProps, addToFavorite } = props;

  console.log(cardItem);

  const { card, img, h1, btnPrice, priceCard, isHeart, heart } = styles;
  return (
    <div className={card}>
      <AiFillHeart
        className={cardItem.isFavourite ? isHeart : heart}
        onClick={() => addToFavorite(cardItem)}
      />
      <h1 className={h1}>{cardItem.name}</h1>;
      <img className={img} src={cardItem.path} alt={cardItem.name} />
      <div className={btnPrice}>
        <div className={priceCard}>{cardItem.price} грн</div>
        <div>
          <Button
            cardItem={cardItem}
            onClose={onClose}
            setModalProps={setModalProps}
            cart="Add to cart"
          />
        </div>
      </div>
    </div>
  );
}

export default CardItem;
