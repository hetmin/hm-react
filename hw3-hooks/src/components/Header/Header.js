import { NavLink } from 'react-router-dom';
import styles from './Header.module.scss';

const Header = () => {
  const { header, nav } = styles;
  return (
    <header className={header}>
      <nav className={nav}>
        <NavLink to="." end>Home</NavLink>
        <NavLink to="/favorite">Favorite</NavLink>
        <NavLink to="/cart">Cart</NavLink>
      </nav>
    </header>
  );
};

export default Header;
