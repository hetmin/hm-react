import React from 'react';
import styles from './Order.module.scss';

const Order = (props) => {
  const { cart, deleteOrder } = props;
  const { container, card, h3, img, btnPrice, priceCard } = styles;

  //   if (Object.keys(cart).length < 10) {
  //     return <h1>ghghg</h1>;
  //   }
  return (
    <div className={container}>
      <div className={card}>
        <img className={img} src={cart.path} alt={cart.name} />
        <h1 className={h3}>{cart.name}</h1>
        <div className={btnPrice}>
          <div className={priceCard}>{cart.price} грн</div>
        </div>
        <button onClick={() => deleteOrder(cart.article)}>Delete</button>
      </div>
    </div>
  );
};

export default Order;
