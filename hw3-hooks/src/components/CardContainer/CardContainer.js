import React from 'react';
import styles from './CardContainer.module.scss';
import CardItem from '../CardItem';

function CardContainer(props) {
  const { cards, onClose, setModalProps, addToFavorite } = props;
  const { list } = styles;

  return (
    <div className={list}>
      {cards.map((cardItem) => {
        return (
          <li key={cardItem.article}>
            <CardItem
              isFavor
              onClose={onClose}
              cardItem={cardItem}
              setModalProps={setModalProps}
              addToFavorite={addToFavorite}
            />
          </li>
        );
      })}
    </div>
  );
}

export default CardContainer;
