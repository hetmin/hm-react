import React from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

function Button(props) {
  const { cart, onClose, cardItem, setModalProps } = props;
  const { button } = styles;
  return (
    <button
      className={button}
      onClick={() => {
        onClose();
        setModalProps(cardItem);
      }}
    >
      {cart}
    </button>
  );
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};

export default Button;
