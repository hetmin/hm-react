import React from 'react';
import styles from './Favorites.module.scss';

const Favorites = (prop) => {
  const { favor } = prop;
  const { container, card, h3, img, btnPrice, priceCard } = styles;
  return (
    <div className={container}>
      <div className={card}>
        <img className={img} src={favor.path} alt={favor.name} />
        <h1 className={h3}>{favor.name}</h1>
        <div className={btnPrice}>
          <div className={priceCard}>{favor.price} грн</div>
        </div>
        {/* <button onClick={() => deleteOrder(favor.article)}>Delete</button> */}
      </div>
    </div>
  );
};

export default Favorites;
