import React from 'react';
import Favorites from '../../components/Favorites';
import styles from './Favorite.module.scss'

function Favorite(props) {
  const { favorite } = props;
  const {list} = styles
  return (
    <>
      <h1>Favorite</h1>
      <main>
        <section className={list}>
          {favorite.map((item) => (
            <Favorites 
              key={item.article}
              favor={item}
              // deleteOrder={deleteOrder}
            />
          ))}
        </section>
      </main>
    </>
  );
}

export default Favorite;
