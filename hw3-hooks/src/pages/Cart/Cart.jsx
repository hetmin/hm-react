import Order from '../../components/Order';

function Cart(props) {
  const { carts, deleteOrder } = props;

  return (
    <>
      <h1>Cart</h1>
      <main>
        <section>
          {carts.map((cart) => (
            <Order key={cart.article} cart={cart} deleteOrder={deleteOrder} />
          ))}
        </section>
      </main>
    </>
  );
}

export default Cart;
