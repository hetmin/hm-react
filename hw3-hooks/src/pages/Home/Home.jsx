import CardContainer from '../../components/CardContainer'
const Home = (props) => {
    const { cards, onClose, setModalProps, addToFavorite } = props;
  return (
    <>
      <main>
        <section>
          <CardContainer
            cards={cards}
            onClose={onClose}
            setModalProps={setModalProps}
            addToFavorite={addToFavorite}
          />
        </section>
      </main>
    </>
  );
};

export default Home;
