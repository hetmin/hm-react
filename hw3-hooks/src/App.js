import { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Modal from './components/Modal';
import Home from './pages/Home/Home';
import Cart from './pages/Cart/Cart';
import Favorite from './pages/Favorite/Favorite';
import NotFound from './pages/NotFound/NotFound';
import MainLayout from './layouts/MainLayout';
import './App.scss';

const API_URL = './fitness.json';

function App() {
  const [cards, setCards] = useState([]);
  const [carts, setCarts] = useState([]);
  const [favorite, setFavorite] = useState([]);
  const [modalProps, setModalProps] = useState({});
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const fetchApi = async () => {
      const res = await fetch(API_URL);
      const data = await res.json();
      setCards(data);

      if (localStorage.getItem('carts')) {
        const carts = await JSON.parse(localStorage.getItem('carts'));
        setCarts(carts);
      }

      if (localStorage.getItem('favorite')) {
        const favorite = await JSON.parse(localStorage.getItem('favorite'));
        setFavorite(favorite);
      }
    };
    fetchApi();
  }, []);

  // useEffect(() => {
  //   const carts = JSON.parse(localStorage.getItem('carts'));
  //   setCarts(carts);
  // }, []);

  // useEffect(() => {
  //   console.log(carts);
  //   localStorage.setItem('carts', JSON.stringify(carts));
  // }, [carts]);

  const showModalHandle = () => {
    setShowModal((showModal) => !showModal);
  };

  const addToCart = (item) => {
    let isInCarts = false;
    carts.forEach((el) => {
      if (el.article === item.article) {
        isInCarts = true;
      }
    });
    if (!isInCarts) {
      setCarts([...carts, item]);
      localStorage.setItem('carts', JSON.stringify(carts));
    }
  };

  const deleteOrder = (id) => {
    setCarts(carts.filter((el) => id !== el.article));
    localStorage.setItem('carts', JSON.stringify(carts));

    if (carts.length === 1) {
      localStorage.removeItem('carts');
    }
  };

  const addToFavorite = (item) => {
    let isInFavorite = false;
    favorite.forEach((el) => {
      if (el.article === item.article) {
        isInFavorite = true;
        setFavorite(
          favorite.filter((el) => {
            return item.article !== el.article;
          })
        );
      }
    });

    setCards(
      cards.map((obj) => {
        if (obj.article === item.article) {
          obj.isFavourite = !obj.isFavourite;
          //  return { ...obj, isFavourite: false };
        }
        return obj;
      })
    );

    if (!isInFavorite) {
      item.isFavourite = true;

      setFavorite([...favorite, item]);
      localStorage.setItem('favorite', JSON.stringify(favorite));
    }
  };

  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<MainLayout />}>
            <Route
              index
              element={
                <Home
                  cards={cards}
                  onClose={showModalHandle}
                  setModalProps={setModalProps}
                  addToFavorite={addToFavorite}
                />
              }
            />
            <Route path="favorite" element={<Favorite favorite={favorite} />} />
            <Route
              path="cart"
              element={<Cart carts={carts} deleteOrder={deleteOrder} />}
            />
            <Route path="*" element={<NotFound />} />
          </Route>
        </Routes>

        {/* <main>
          <section>
            <CardContainer
              cards={cards}
              onClose={showModalHandle}
              setModalProps={setModalProps}
            />
          </section>
        </main> */}

        <Modal
          show={showModal}
          onClose={showModalHandle}
          closeButton={true}
          text={'Додати в кошик ' + modalProps.name + ' ?'}
          actions={
            <>
              <button
                className="btn"
                onClick={() => {
                  showModalHandle();
                  addToCart(modalProps);
                }}
              >
                Ok
              </button>
              <button className="btn" onClick={showModalHandle}>
                Cancel
              </button>
            </>
          }
        ></Modal>
      </div>
    </BrowserRouter>
  );
}

export default App;
