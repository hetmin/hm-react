import { PureComponent } from 'react';
import { BsCartCheckFill } from 'react-icons/bs';
import { AiFillHeart } from 'react-icons/ai';
import styles from './Header.module.scss';
class Header extends PureComponent {
  render() {
    const { header, headerIcon, headerIconCart } = styles;
    let cartQt;
    let favouriteQt;
    if (localStorage.getItem('carts')) {
      cartQt = <span>{JSON.parse(localStorage.getItem('carts')).length}</span>;
    } else {
      cartQt = <span>0</span>;
    }

    if (localStorage.getItem('favourites')) {
      favouriteQt = (
        <span>{JSON.parse(localStorage.getItem('favourites')).length}</span>
      );
    } else {
      favouriteQt = <span>0</span>;
    }

    return (
      <header className={header}>
        <h1>Fitness</h1>
        <ul>
          <li>
            <a href="#">
              <BsCartCheckFill className={headerIconCart} />
            </a>
          </li>
          <li>{cartQt}</li>
        </ul>
        <ul>
          <li>
            <a href="#">
              <AiFillHeart className={headerIcon} />
            </a>
          </li>
          <li>{favouriteQt}</li>
        </ul>
      </header>
    );
  }
}

export default Header;
