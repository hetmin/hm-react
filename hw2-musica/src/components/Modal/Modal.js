import React from 'react';
import styles from './Modal.module.scss';

class Modal extends React.PureComponent {
  render() {
    const { closeButton, actions, close, modalProps } = this.props;
    const {
      modalBackground,
      modalContent,
      modalContentHeader,
      modalCloseBtn,
      modalContentText,
      modalButtons,
    } = styles;
    return (
      <div className={modalBackground} onClick={close}>
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
          className={modalContent}
        >
          <header className={modalContentHeader}>
            {closeButton && (
              <div onClick={close} className={modalCloseBtn}>
                &times;
              </div>
            )}
          </header>
          <div>
            <p className={modalContentText}>
              Додати товар {modalProps.name} в кошик?
            </p>
            <div className={modalButtons}>{actions}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
