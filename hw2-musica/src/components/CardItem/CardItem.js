import { PureComponent } from 'react';
import { AiFillHeart } from 'react-icons/ai';
import styles from './CardItem.module.scss';
import Button from '../Button';
import PropTypes from 'prop-types';
class CardItem extends PureComponent {
  render() {
    const {
      name,
      path,
      price,
      article,
      favourite,
      showModal,
      setModalProps,
      addToFavourites,
    } = this.props;

    const {
      card,
      likeButton,
      title,
      itemAvatar,
      description,
      btnContainer,
      buttons,
      iconAdd,
      isIconAdd,
    } = styles;
    return (
      <div className={card}>
        <button type="button" className={likeButton}>
          <AiFillHeart
            className={favourite ? isIconAdd : iconAdd}
            onClick={() => {
              setModalProps({ article });
              addToFavourites(article);
            }}
          />
        </button>
        <span className={title}>{name}</span>
        <img className={itemAvatar} src={path} alt={name} />
        <span className={description}>{price} грн</span>

        <div className={btnContainer}>
          <div className={buttons}>
            <Button
              backgroundColor="rgba(231, 92, 92, 0.978)"
              text="Add to cart"
              className="modalButton"
              onClick={() => {
                setModalProps({ article, name });
                showModal();
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CardItem;

CardItem.propTypes = {
  name: PropTypes.string,
  path: PropTypes.string,
  price: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  favourite: PropTypes.bool,
  showModal: PropTypes.func,
  setModalProps: PropTypes.func,
  addToFavourites: PropTypes.func,
};
