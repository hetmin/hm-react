import React, { PureComponent } from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

class Button extends PureComponent {
  render() {
    const { backgroundColor, text, onClick } = this.props;
    const { modalButton } = styles;
    return (
      <button
        className={modalButton}
        style={{ backgroundColor }}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

export default Button;

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
};
