import React, { PureComponent } from 'react';
import CardItem from '../CardItem';
import styles from './CardContainer.module.scss';

class CardContainer extends PureComponent {
  render() {
    const {
      addToCart,
      cards,
      showModal,
      setModalProps,
      addToFavourites,
      favourites,
    } = this.props;

    const { list } = styles;

    return (
      <div>
        <ul className={list}>
          {cards.map(({ name, path, price, article, isFavourite }) => (
            <li key={article}>
              <CardItem
                favourite={favourites.includes(article)}
                addToCart={addToCart}
                article={article}
                name={name}
                price={price}
                path={path}
                isFavourite={isFavourite}
                showModal={showModal}
                setModalProps={setModalProps}
                addToFavourites={addToFavourites}
              />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default CardContainer;
