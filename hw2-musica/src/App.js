import React from 'react';
import './App.css';
import Header from './components/Header';
import CardContainer from './components/CardContainer';
import Modal from './components/Modal';
import Button from './components/Button';
const API_URL = './fitness.json';

class App extends React.Component {
  state = {
    cards: [],
    carts: [],
    show: false,
    favourites: [],
    modalProps: {},
    isLoaded: false,
    error: null,
  };

  setModalProps = (value) => {
    this.setState({ modalProps: value });
  };

  async componentDidMount() {
    try {
      const cards = await fetch(API_URL).then((res) => res.json());

      this.setState({ cards: cards, isLoaded: true });
      if (localStorage.getItem('carts')) {
        const carts = await JSON.parse(localStorage.getItem('carts'));
        this.setState({ carts });
      }
      if (localStorage.getItem('favourites')) {
        const favourites = await JSON.parse(localStorage.getItem('favourites'));
        this.setState({ favourites });
      }
    } catch (error) {
      this.setState({
        isLoaded: true,
        error: error,
      });
    }
  }

  showModal = () => {
    this.setState({ show: !this.state.show });
  };

  addToCart = (article) => {
    this.setState((current) => {
      const carts = [...current.carts];
      const index = carts.findIndex((el) => el.article === article);

      if (index === -1) {
        carts.push({ ...article, count: 1 });
      } else {
        carts[index].count += 1;
      }

      localStorage.setItem('carts', JSON.stringify(carts));
      this.showModal();
      return { carts };
    });
  };

  addToFavourites = (article) => {
    this.setState((current) => {
      let favourites = [...current.favourites];
      if (current.favourites.includes(article)) {
        favourites = current.favourites.filter((el) => {
          return el !== article;
        });
      } else {
        favourites = [...current.favourites, article];
      }
      localStorage.setItem('favourites', JSON.stringify(favourites));
      return { favourites };
    });
  };

  render() {
    const { show, cards, carts, modalProps, favourites, isLoaded, error } =
      this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    }
    return (
      <>
        <Header carts={carts} favourites={favourites} />
        <main>
          <section>
            <h1>Товари для фітнесу</h1>
            <CardContainer
              favourites={favourites}
              addToCart={this.addToCart}
              cards={cards}
              carts={carts}
              showModal={this.showModal}
              setModalProps={this.setModalProps}
              addToFavourites={this.addToFavourites}
            />
          </section>
        </main>

        {show && (
          <Modal
            className="modal"
            closeButton={true}
            close={this.showModal}
            modalProps={modalProps}
            addToCart={this.addToCart}
            actions={[
              <Button
                key={1}
                onClick={() => {
                  this.addToCart(modalProps.article);
                }}
                className="modal-button"
                text="Add"
              />,
              <Button
                key={2}
                onClick={this.showModal}
                className="modal-button"
                text="Cancel"
              />,
            ]}
          />
        )}
      </>
    );
  }
}

export default App;
