import React from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import './App.scss';

class App extends React.Component {
  state = {
    show1: false,
    show2: false,
  };

  showModal1 = () => {
    this.setState({ show1: !this.state.show1 });
  };

  showModal2 = () => {
    this.setState({ show2: !this.state.show2 });
  };

  onClose = (e) => {
    this.props.show = false;
  };

  render() {
    // const btn = (
    //   <button
    //     onClick={(e) => {
    //       this.showModal2(e);
    //     }}
    //   >
    //     Close
    //   </button>
    // );
    const modalTextHeader = 'Do you want to delete this file?';
    const modalText =
      'Once you delete this file. Are you sure you want to delete it';
    const modalTextHeader2 = 'This is modal window'
    const modalText2 = 'Delete this window?'

    return (
      <div className="App">
        <Modal
          show={this.state.show1}
          onClose={this.showModal1}
          closeButton={true}
          header={modalTextHeader2}
          text={modalText2}
          actions={
            <>
              <button className="btn" onClick={this.showModal1}>
                Ok
              </button>
              <button className="btn" onClick={this.showModal1}>
                Cancel
              </button>
            </>
          }
        ></Modal>
        <Button
          showModal={this.showModal1}
          name="Open first modal"
          backgroundColor="lightcoral"
          text="20px"
        />
        <Modal
          show={this.state.show2}
          onClose={this.showModal2}
          closeButton={true}
          header={modalTextHeader}
          text={modalText}
          actions={
            <>
              <button className="btn" onClick={this.showModal2}>
                Ok
              </button>
              <button className="btn" onClick={this.showModal2}>
                Cancel
              </button>
            </>
          }
        ></Modal>

        <Button
          showModal={this.showModal2}
          name="Open second modal"
          backgroundColor="lightgreen"
          text="20px"
        />
      </div>
    );
  }
}

export default App;
