import React from 'react';
import styles from './Modal.module.scss';

class Modal extends React.Component {
  render() {
    const {
      modal,
      modal__body,
      modal__header,
      modal__headerClose,
      modal__main,
      modal__footer,
    } = styles;
    const { show, header, text, actions, onClose } = this.props;
    if (!show) {
      return null;
    }
    return (
      <div className={modal} onClick={onClose}>
        <div className={modal__body} onClick={(e) => e.stopPropagation()}>
          <div className={modal__header}>
            <h2>{header}</h2>
            <div className={modal__headerClose} onClick={onClose}>
              ×
            </div>
          </div>

          <div className={modal__main}>{text}</div>

          <div className={modal__footer}>{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
