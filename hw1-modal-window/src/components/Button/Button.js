import React from 'react';
import styles from './Button.module.scss';

class Button extends React.Component {
  render() {
    const {button} = styles
    const { name, showModal } = this.props;
    return (
      <>
        <button
          className={button}
          onClick={showModal}
          style={{
            backgroundColor: this.props.backgroundColor,
            fontSize: this.props.text,
          }}
        >
          {name}
        </button>
      </>
    );
  }
}

export default Button;
